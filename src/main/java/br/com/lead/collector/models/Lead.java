package br.com.lead.collector.models;

import br.com.lead.collector.enums.TipoLeadEnum;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Entity
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 5, max = 30, message = "Nome deve ter entre 5 e 30 caracteres")
    private String nome;

    @Email(message = "Formato de e-mail inválido")
    @NotNull(message = "E-mail não pode ser nullo")
    @NotBlank(message = "E-mail não pode ficar em branco")
    private String email;

    @Null(message = "Data não pode ser informada")
    private LocalDate data;
    private TipoLeadEnum tipoLead;

    public Lead(){
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipoLeadEnum tipoLead) {
        this.tipoLead = tipoLead;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
